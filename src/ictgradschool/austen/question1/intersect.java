package ictgradschool.austen.question1;


public class    intersect {

    private String[] intersection(String[] arr1, String[] arr2) {

        int numCommonValues = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {

                if (arr1[i].equals(arr2[j])) {
                    numCommonValues++;
                    break;
                }

            }
        }

        String[] result = new String[numCommonValues];
        int counter = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {

                if (arr1[i].equals(arr2[j])) {
                    result[counter++] = arr1[i];
                    break;
                }

            }
        }

        return result;

    }
}
